#include <stdio.h>
int main()
{
    int i,n,s=0,l=0,SP=0,LP=0;
    printf("Enter the number of elements:\n");
    scanf("%d",&n);
    int a[n];
    for(i=0;i<n;i++)
    {
        printf("Enter the %d element =\n",i);
        scanf("%d",&a[i]);
    }
    for(i=0;i<n;i++)
    {
        printf("The a[%d] element = %d\n",i,a[i]);
    }
    s=a[0];
    for(i=1;i<n;i++)
    {
        if(s>a[i])
         {
            s=a[i];
            SP=i;
         }
        if(l<a[i])
         {
            l=a[i];
            LP=i;
         }
    }
    printf("\nThe smallest element is %d and its position is %d",s,SP);
    printf("\nThe largest element is %d and its position id %d",l,LP);

    printf("\nThe interchanged arrangement is as follows: ");

    a[SP]=l;
    a[LP]=s;
    for(i=0;i<n;i++)
        {
        printf("\nThe a[%d] element = %d",i,a[i]);
        }
    return 0;
}
