#include <stdio.h>
int main()
{
    struct library
    {
        char bookname[20];
    };
    struct student
    {
        int rollno;
        char name[10];
        char depart[10];
        struct library book;
    };
    struct student S1;
    printf("Enter your roll number=\n");
    scanf("%d",&S1.rollno);
    printf("Enter your name=\n");
    scanf("%s",&S1.name);
    printf("Enter which department you belong to=\n");
    scanf("%s",&S1.depart);
    printf("Enter the book you have borrowed=\n");
    scanf("%s",&S1.book.bookname);

    printf("STUDENT DETAILS\n");

    printf("ROLL No. = %d",S1.rollno);
    printf("\nNAME = %s\n",S1.name);
    printf("DEPARTMENT = %s\n",S1.depart);
    printf("TITLE OF THE BOOK BORROWED = %s",S1.book.bookname);
    return 0;