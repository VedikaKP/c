#include <stdio.h>
int main()
{
    int i,n,key,f;
    printf("Enter the elements=\n");
    scanf("%d",&n);
    int a[n];

    for(i=0;i<n;i++)
    {
        printf("Enter %d element=\n",i);
        scanf("%d",&a[i]);
    }

    printf("Enter the number to be searched\n");
    scanf("%d",&key);

    int l=0, h=n-1,m;
    while(l<=h)
    {
        m=(l+h)/2;
        if(key==a[m])
        {
            f=1;
            printf("%d is found at %d",key,m+1);
            break;
        }
        if(key>a[m])
            l=m+1;
        else
            h=m-1;
    }
    return 0;
}
