#include <stdio.h>
int input();
int fac(int);
void output(int,int);
int main()
{
    int n,i,res;
    n=input();
    res=fac(n);
    output(res,n);
    return 0;
}
int input()
{
    int x;
    printf("Enter a number");
    scanf("%d",&x);
    return x;
}
int fac(int n)
{
    int f=1;
    for(int i=1;i<=n;i++)
    {
        f = f*i;
    }
    return f;
}
void output(int res, int n)
{
    printf("The factorial of a %d = %d",n,res);
}