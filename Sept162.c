#include <stdio.h>

int input();
int convert(int,int);
void output(int);
int main()
{
    int a,b,result;
    a = input();
    b = input();
    result = convert(a,b);
    output(result);
}
int input()
{
    int x;
    printf("Enter the hours and minutes");
    scanf("%d",&x);
    return x;
}
int convert(int a, int b)
{
    return (a*60)+b;
}
void output (int result)
{
    printf("The required time is= %d",result);
}