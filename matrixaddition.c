#include <stdio.h>
int main()
{
    int r1,r2,c1,c2,i,j;
    printf("Enter the numbers of rows and columns in first matrix\n");
    scanf("%d %d",&r1,&c1);
    printf("Enter the numbers of rows and columns in second matrix\n");
    scanf("%d %d",&r2,&c2);
    if(r1!=r2 || c1!=c2)
    {
        printf("The operation cannot be performed");
        exit(0);
    }
    int a[r1][c1];
    printf("\n Enter the values for the first matrix");
    for(i=0;i<r1;i++)
    {
        for(j=0;j<c1;j++)
        {
           printf("\nEnter the a[%d][%d] element",i,j);
           scanf("%d",&a[i][j]);
        }
    }
    int b[r2][c2];
    printf("\n Enter the values for the second matrix");
    for(i=0;i<r2;i++)
    {
        for(j=0;j<c2;j++)
        {
           printf("\nEnter the b[%d][%d] element",i,j);
           scanf("%d",&b[i][j]);
        }
    }

    printf("\n The resultant sum of the addition:\n");
    int c[r1][c1];
    for(i=0;i<r2;i++)
    {
        for(j=0;j<c2;j++)
        {
           c[i][j] = a[i][j]+b[i][j];
        }
    }
    for(i=0;i<r1;i++)
    {
        for(j=0;j<c1;j++)
        {
           printf("\t %d",c[i][j]);
        }
        printf("\n");
    }
    return 0;
}
