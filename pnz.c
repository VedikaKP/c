#include <stdio.h>
int input();
int compare(int);
void output(int);
int main()
{
    int a,res;
    a=input();
    res=compare(a);
    output(res);
    return 0;
}
int input()
{
    int x;
    printf("Enter a number");
    scanf("%d",&x);
    return x;
}
int compare(int a)
{
    if(a>0)
        return 0;
    else if(a<0)
        return 1;
    else 
        return 2;
}
void output(int res)
{
    if(res==0)
        printf("The number is positive");
    else if(res==1)
        printf("The number is negative");
    else 
        printf("The number is zero");
}