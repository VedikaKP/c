#include <stdio.h>
int main()
{
int i,n,num=0,pos=0;
printf("Enter the number of elements:\n");
scanf("%d",&n);
int a[n];
for(i=0;i<n;i++)
{
    printf("Enter a %d elements:\n",i);
    scanf("%d",&a[i]);
}
for(i=0;i<n;i++)
{
    printf("The a[%d] element = %d\n",i,a[i]);
}
printf("Enter the element to be deleted:\n");
scanf("%d",&num);
printf("Enter the position at which the element has to be deleted:\n");
scanf("%d",&pos);

for(i=pos;i<n-1;i++)
{
    a[i]=a[i+1];
}
n--;

printf("\n The array after the deletion of %d is\n",num);
for(i=0;i<n;i++)
{
    printf("a[%d]=%d\n",i,a[i]);
}
return 0;
}
