    #include <stdio.h>
    #include <math.h>
    int input();
    int roots(int,int,int);
    void output(int,int,int,int,int);
    int main()
{
    int a,b,c,res,D;
    a=input();
    b=input();
    c=input();
    res=roots(a,b,c);
    output(res,a,b,c,D);
    return 0;
}
    int input()
{
    int x;
    printf("Enter a number\n");
    scanf("%d",&x);
    return x;
}
    int roots(int a,int b, int c)
{   
    int D;
    D = (b*b)-(4*a*c);
    if (D==0)
    return 0;
    else if (D>0)
    return 1;
    else 
    return 2;
}
    void output(int res,int a, int b,int c,int D)
{
    if(res==0)
    printf("Roots are real and equal and are given as = %f %f",(float)((-b+sqrt(D))/(2*a)),(float)((-b-sqrt(D))/(2*a)));
    else if(res==1)
    printf("Roots are real and distinct and are given as = %f %f",(float)((-b+sqrt(D))/(2*a)),(float)((-b-sqrt(D))/(2*a)));
    else 
    printf("Roots are imaginary");  
}