#include <stdio.h>
int input();
float compute(int);
void output(int,float);
int main()
{
    int n,i,sum=0;
    float avg=0.0,res;
    n=input();
    res=compute(n);
    output(n,res);
    return 0;
}
int input()
{
    int x;
    printf("Enter a number");
    scanf("%d",&x);
    return x;
}
float compute(int n)
{
    int sum = 0;
    float avg=0.0;
    for(int i=1;i<=n;i++)    
    {
        sum+=i;
    }
    avg=(float)sum/n;
    return avg;
}
void output(int n, float res)
{
    printf("The average of first %d numbers = %f",n,res);
}