#include <stdio.h>
int input();
int sqr(int,int,int);
void output(int,int);
int main()
{
    int i,n,sum=0,res;
    n=input();
    res = sqr(n,i,sum);
    output(res,n);
    return 0;
}
int input()
{
    int x;
    printf("Enter a number");
    scanf("%d",&x);
    return x;
}
int sqr(int n,int i,int sum)
{
    for(i=2;i<n;i+=2)
    {
        sum+=(i*i);
    }
    return sum;
}
void output(int res,int n)
{
    printf("The sum of squares of even numbers within %d = %d",n,res);
}

